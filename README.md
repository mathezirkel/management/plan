# Plan

This is a Python tool for planning the schedule of the Mathecamp of the Matheschülerzirkel Augsburg.

# Credits

Plan uses the [The SCIP Optimization Suite 7.0](http://www.optimization-online.org/DB_HTML/2020/03/7705.html)
together with its Python wrapper [PySCIPOpt](https://github.com/SCIP-Interfaces/PySCIPOpt).

# Licence

All code from Plan is licenced under [GPL-3](LICENSE).