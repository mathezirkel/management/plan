#!/usr/bin/env bash
# Script for building a Debian docker image with SCIP and Python 3.6 installed

VERSION="7.0.0"

if [ ! -f SCIPOptSuite-7.0.0-Linux.deb ]; then
  echo "Debian package for the SCIP optimization suite not found. Pleas download it from https://scip.zib.de/index.php#download."
fi

if [ ! -x "$(command -v docker)" ]; then
  echo "Couldn't find the command docker. Is Docker installed?"
fi

docker build -t registry.gitlab.com/mathezirkel/plan/python36_scip:$VERSION .

echo "Built image registry.gitlab.com/mathezirkel/plan/python36_scip:$VERSION"
