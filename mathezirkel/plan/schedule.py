#  Plan is a tool to create time tables for the Mathecamp Augsburg.
#  Copyright (c) 2020-2020 Matheschülerzirkel Augsburg
#
#  This program is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program.  If not, see <https://www.gnu.org/licenses/>.
from typing import List, Dict, Set, Tuple

from pyscipopt.scip import Model, Variable, quicksum, Expr


def create_time_table(
        sessions: List[str],
        circles: Dict[str, List[int]],
        tutors: List[str],
        outages: Dict[str, List[str]],
        preferences: Dict[str, Dict[int, int]]) -> Dict[str, Dict[str, str]]:
    """
    Create a time table from given data.

    Note that the keys of `circles` is supposed to be a list of all circles to schedule.

    :param sessions: A list of all sessions to schedule.
    :param circles: A dictionary of all circles to schedule together with its associated grade..
    :param tutors: A list of all tutors to schedule.
    :param outages: For every tutor one can specify a list of sessions that are not to be used for that tutor.
    :param preferences: For every tutor one may specify which class is preferred by the tutor
     (with a positive value) and which is disliked by the tutor (with a negative value)
    :return: A timetable, that is for every session one specifies which circle has which tutor.
    """
    # Validate arguments
    verify_time_table_arguments(sessions, circles, tutors, outages, preferences)

    # Create model and add variables
    model, variables, p_pref, p_grade, p_pause = define_model_with_variables(sessions, list(circles.keys()), tutors)

    # Add constraints
    add_constraint_every_circle_is_covered(model, variables)
    add_constraint_every_tutor_has_at_most_one_circle(model, variables)
    add_constraint_outages(model, variables, outages)
    add_constraint_every_tutor_is_scheduled(model, variables)
    add_constraint_for_preferences_objective(model, variables, circles, preferences, p_pref)
    add_constraint_for_grade_objective(model, variables, circles, p_grade)

    # Set objective
    alpha_pref = 1
    alpha_grade = 1
    alpha_pause = 1
    model.setObjective(alpha_pref * p_pref + alpha_grade * alpha_grade + alpha_pause * p_pause)

    # Solve problem
    model.optimize()
    solution = model.getBestSol()

    # Interpret and return result
    result = interpret_result(variables, solution)
    return result


def verify_time_table_arguments(
        sessions: List[str],
        circles: Dict[str, List[int]],
        tutors: List[str],
        outages: Dict[str, List[str]],
        preferences: Dict[str, Dict[int, int]]) -> None:
    # All circles have existing grades
    for circle in circles.values():
        for grade in circle:
            if grade < 5 or grade > 13:
                raise ValueError(f"Circle {circle} is for grade {grade} which is below 5 or above 13")
    # All outages consist of existing tutors and sessions
    for tutor, tutor_sessions in outages.items():
        if tutor not in tutors:
            raise ValueError(f"Tutor {tutor} is referenced in outages but is not contained in tutors")
        for session in tutor_sessions:
            if session not in sessions:
                raise ValueError(
                    f"Session {session} is referenced in as an outage of {tutor} but is not contained in sessions")

    # All preferences consist of existing tutors and grades
    grades: Set[int] = {grade for circle_grade in circles.values() for grade in circle_grade}
    for tutor, grade_preferences in preferences.items():
        if tutor not in tutors:
            raise ValueError(f"Tutor {tutor} is referenced in preferences but is not contained in tutors")
        for grade_to_check in grade_preferences.keys():
            if grade_to_check not in grades:
                raise ValueError(
                    f"Grade {grade_to_check} was referenced as preference of {tutor}" +
                    f" but there does not appear to be any circle with this grade in circles")


def define_model_with_variables(
        sessions: List[str],
        circles: List[str],
        tutors: List[str]) -> Tuple[Model, Dict[str, Dict[str, Dict[str, Variable]]], Variable, Variable, Variable]:
    """
    Create a PySCIP model by adding respective binary variables to it, as defined by the inputs.

    The result contains not just the model but also a triple-nested dictionary to associate input variables
    with their associated PySCIP variable. Furthermore, it returns three additional variables
    corresponding to the summands for the objective function.

    :param sessions: List of names of sessions to consider.
    :param circles: List of circles to consider.
    :param tutors: List of tutors to consider.
    :return: A 5-tuple containing
        - the PySCIP model with all the variables added
        - a triple nested dictionary associating for finding a PySCIP variable corresponding to a combination
          of a (circle, session, tutor), in that order. Notice that this is the same order as described in `design.adoc`
        - three more variables corresponding to `p_pref`, `p_grade` and `p_pause` in that order as desccribed in `design.adoc`
    """
    model: Model = Model("Mathecamp Scheduling Problem")
    variables: Dict[str, Dict[str, Dict[str, Variable]]] = {}
    for circle in circles:
        variables[circle]: Dict[str, Dict[str, Variable]] = {}
        for session in sessions:
            variables[circle][session]: Dict[str, Variable] = {}
            for tutor in tutors:
                variables[circle][session][tutor] = model.addVar(f"({circle},{session},{tutor})", vtype="BINARY")
    p_pref: Variable = model.addVar("p_pref", vtype="INTEGER", lb=0)
    p_grade: Variable = model.addVar("p_grade", vtype="INTEGER", lb=0)
    p_pause: Variable = model.addVar("p_pause", vtype="INTEGER", lb=0)
    return model, dict(variables), p_pref, p_grade, p_pause


def add_constraint_every_circle_is_covered(
        model: Model,
        variables: Dict[str, Dict[str, Dict[str, Variable]]]) -> None:
    """
    Add constraints to a model to ensure that every circle has exactly one
    tutor during any session.

    :param model: Model to add constraints to.
    :param variables: Variables of `model`. Arguments are (circle, session, tutor).
    :return: None
    """
    for circle, timetable in variables.items():
        for session, tutors in timetable.items():
            model.addCons(quicksum(tutors.values()) == 1,
                          name=f"Circle {circle} must have exactly one tutor at session {session}")


def get_indices_from_variables(variables: Dict[str, Dict[str, Dict[str, Variable]]]) -> Tuple[
    List[str], List[str], List[str]]:
    """
    Get the actual indices from a dictionary of variables.

    :param variables: Nested dictionary containing variables indexed in the order (circle, session, tutor)
    :return: A triple of lists of indices of `variables` in the order (circle, session, tutor)
    """
    circles: List[str] = list(variables.keys())
    sessions: List[str] = list(variables[circles[0]].keys())
    tutors: List[str] = list(variables[circles[0]][sessions[0]].keys())
    return circles, sessions, tutors


def get_circles_per_grade(circles: Dict[str, List[int]]) -> Dict[int, List[str]]:
    """
    Get a list of circles per grade.

    :param circles: A dictionary of grades per circle.
    :return: A dictionary of circles per grade.
    """
    result: Dict[int, List[str]] = {}
    for circle, grades in circles.items():
        for grade in grades:
            if grade not in result.keys():
                result[grade] = []
            if circle not in result[grade]:
                result[grade].append(circle)
    return result


def add_constraint_every_tutor_has_at_most_one_circle(
        model: Model,
        variables: Dict[str, Dict[str, Dict[str, Variable]]]) -> None:
    """
    Add constraints to a model to ensure that every tutor has at most one
    circle during any session.

    :param model: Model to add constraints to.
    :param variables: Variables of `model`. Arguments are (circle, session, tutor).
    :return: None
    """
    circles, sessions, tutors = get_indices_from_variables(variables)
    for session in sessions:
        for tutor in tutors:
            model.addCons(quicksum([variables[circle][session][tutor] for circle in circles]) <= 1,
                          name=f"Tutor {tutor} must have at most one circle at session {session}")


def add_constraint_outages(
        model: Model,
        variables: Dict[str, Dict[str, Dict[str, Variable]]],
        outages: Dict[str, List[str]]) -> None:
    """
    Add constraints to a model to ensure that outages are respected.

    :param model: Model to add constraints to.
    :param variables: Variables of `model`. Arguments are (circle, session, tutor).
    :param outages: A dictionary associating tutors with lists of sessions that they are unavailable.
    :return: None.
    """
    circles, sessions, tutors = get_indices_from_variables(variables)
    for tutor, tutor_outages in outages.items():
        for session in tutor_outages:
            for circle in circles:
                model.addCons(variables[circle][session][tutor] == 0,
                              name=f"Tutor {tutor} has an outage at session {session} for circle {circle}")


def add_constraint_every_tutor_is_scheduled(
        model: Model,
        variables: Dict[str, Dict[str, Dict[str, Variable]]]) -> None:
    """
    Add constraints to a model to ensure that every tutor has at least one circle session.

    :param model: Model to add constraints to.
    :param variables: Variables of `model`. Arguments are (circle, session, tutor).
    :return: None.
    """
    circles, sessions, tutors = get_indices_from_variables(variables)
    for tutor in tutors:
        model.addCons(quicksum([variables[circle][session][tutor] for circle in circles for session in sessions]) >= 1,
                      name=f"Tutor {tutor} has at least one circle session")


def add_constraint_for_preferences_objective(
        model: Model,
        variables: Dict[str, Dict[str, Dict[str, Variable]]],
        circles: Dict[str, List[int]],
        preferences: Dict[str, Dict[int, int]],
        p_pref: Variable) -> None:
    """
    Add constraints to a model to map the objective of adhering to preferences to an auxiliary variable.

    :param model: Model to add constraints to.
    :param variables: Variables of `model`. Arguments are (circle, session, tutor).
    :param circles: Map of circles to grades.
    :param preferences: A list of tutors together with an integer weight for each grade.
    :param p_pref: Auxiliary variable that shall be set equal to the actual objective contribution.
    :return: None.
    """
    circle_names, sessions, tutors = get_indices_from_variables(variables)
    objective: Expr = Expr()
    for tutor, grades in preferences.items():
        for grade, weight in grades.items():
            objective = objective + 1
            for circle, circle_grades in circles.items():
                if grade in circle_grades:
                    for session in sessions:
                        objective = objective - weight * variables[circle][session][tutor]

    model.addCons(objective == p_pref, name=f"Auxiliary variable p_pref is set to objective function")


def variance(arguments: List[Expr]) -> Expr:
    """
    Map a list of expressions to their not-normalized variance.

    :param arguments: List of linear expressions to sum over.
    :return: Expressions equal to the sum of squares of the differences of the items in `arguments`.
    """
    result: Expr = Expr()
    for i in range(0, len(arguments) - 1):
        for j in range(i + 1, len(arguments)):
            result + (arguments[i] - arguments[j]) * (arguments[i] - arguments[j])
    return result


def add_constraint_for_grade_objective(
        model: Model,
        variables: Dict[str, Dict[str, Dict[str, Variable]]],
        circles: Dict[str, List[int]],
        p_grade: Variable) -> None:
    """
    Add constraints to a model to map the objective function for uniform distribution of sessions among circles
    of the same grade to an auxiliary variable.

    :param model: Model to add constraints to.
    :param variables: Variables of `model`. Arguments are (circle, session, tutor).
    :param circles: Map of circles to grades.
    :param p_grade: Auxiliary variable that shall be set equal to the actual objective contribution.
    :return: None.
    """
    circle_names, sessions, tutors = get_indices_from_variables(variables)
    objective: Expr = Expr()
    for tutor in tutors:
        for grade, circles_in_grade in get_circles_per_grade(circles).items():
            objective = objective + variance(
                [quicksum([variables[circle][session][tutor] for session in sessions]) for circle in circles_in_grade])

    model.addCons(objective == p_grade, name=f"Auxiliary variable p_grade is set to objective function")


def get_available_sessions(
        tutors: List[str],
        number_of_sessions: int,
        outages: Dict[str, List[str]]) -> Dict[str, int]:
    return {tutor: number_of_sessions - len(outages[tutor]) for tutor in tutors}


def add_constraint_for_pause_objective(
        model: Model,
        variables: Dict[str, Dict[str, Dict[str, Variable]]],
        p_pause: Variable) -> None:
    """
    Add constraints to a model to map the objective function for a uniform distribution of sessions among circles
    of the same grade to an auxiliary variable.

    TODO This is actually wrong at the moment because tutors with fewer hours should also have fewer breaks.

    :param model: Model to add constraints to.
    :param variables: Variables of `model`. Arguments are (circle, session, tutor).
    :param p_pause: Auxiliary variable that shall be set equal to the actual objective contribution.
    :return: None.
    """
    circles, sessions, tutors = get_indices_from_variables(variables)
    objective: Expr = variance(
        [quicksum([variables[circle][session][tutor] for session in sessions for circle in circles]) for tutor in
         tutors]
    )

    model.addCons(objective == p_pause, name=f"Auxiliary variable p_pause is set to objective function")


def interpret_result(
        variables: Dict[str, Dict[str, Dict[str, Variable]]],
        solution) -> Dict[str, Dict[str, str]]:
    """
    Transform the result of a PySCIP optimization into a human-readable dictionary.

    :param variables: Variables of the problem.
    :param solution: Solution determined by SCIP.
    :return: A dictionary that maps every session to a dictionary mapping a circle to a tutor.
    """
    circles, sessions, tutors = get_indices_from_variables(variables)
    result: Dict[str, Dict[str, str]] = {}
    for session in sessions:
        result[session] = {}
        for circle in circles:
            for tutor in tutors:
                if solution[variables[circle][session][tutor]] == 1.0:
                    result[session][circle] = tutor
    return result
